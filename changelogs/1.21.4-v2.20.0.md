# 1.21.4-v2.20.0

## General Changes
- We're on 1.21.4. With that comes the general expected changes, like item data components. 

## Building Tool
- Clone: Now works across dimensions and players. You can hand another player a building tool and access their cloned region regardless of what dimension it's from.
- Added a nifty animation when the GUI opens :D

## Gel Blocks
- Gel blocks have had their back end rewritten from scratch.
- Previously, Gel Blocks implemented the `IStructureGel` interface to hook into the various spreading mechanics, used an enum of behaviors to determine the default spreading rules, and had a block state for how far it traveld. Most of this is gone.
- Gel spreading behavior is now registered under `structure_gel:gel_spread_behavior`.
	- This is how gel spreads along a specific axis, or along diagonals.
- Gel spreading restrictions can be registered under `structure_gel:gel_spread_restriction`.
	- This is how blue gel knows not to spread into exposed areas, or how orange gel knows to stop based on the item stack count.
- The distance gel spreads is now an absolute distance to the initial source instead of traveling and counting how many were placed, and reaches a sphere shape if fully expanded.
	- This allows gel to spread more effectively in twisting hallways, and more reliably hit the corners of large rooms.
- Any block can be used as gel in a structure as long as it's added to the `structure_gel:gel` block tag. If the block in the tag is actually a `StructureGelBlock`, it can customize what it places instead of defaulting to air.

## Data Handler
- Changed how registry works so that a builder is what gets registered. See implemented data handlers for reference.

## Dynamic Spawner
- Changed how registry works so that a builder is what gets registered.

## `GelPortalBlock`
- Completely rewritten. Hopefully for the last time. The `GelPortalBlock` class is abstract and requires the implementation of various methods to make the portal function properly (with similar behavior to a nether portal).
	- Ignition of the portal is handled by calling `GelPortalBlock.tryIgnite`.
	- The dimension transition screen must be registered manually.

## Registries
- Added two new registries; `structure_gel:gel_spread_behavior` and `structure_gel:gel_spread_restriction`.
	- Used to control the behavior of Gel blocks. Custom values can be registered and implemented on your own custom Gel block.
- Changed the registry process of `structure_gel:dynamic_spawner_type` and `structure_gel:data_handler_type`.
	- The new registry approach instead works by registering a "factory" to create a Data Handler or Dynamic Spawner. Overall, the code is more builder-focused and simplified, with access to registry data to prepare for more registries moving to datapacks in vanilla.
	- See `com.legacy.structure_gel.core.registry.SGRegistries` for how to register the new data.

## Registrar
- Internal changes to how data is stored. The basic rules of "don't get a value before it was registered" still applies.
- `Registrar.Static` now contains a `Holder.Reference` for the object registered.
- `Registrar.Static` can be created with a name only using `RegistrarHandler.named(String)`.
	- In this case, a value must be assigned later through `RegistrarHandler.addHandlerListener(Consumer<RegistrarHandler>)`.
	- An error will be thrown if a static registrar is created by name only and never assigned a value.
- Added updated handling for block and item registry.
- See `com.legacy.structure_gel.core.registry.SGRegistries` for usage.